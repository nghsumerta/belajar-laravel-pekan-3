<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">

    <title>SanberBook | Register</title>
  </head>
  <body>

    <div class="container-fluid">
    <h1>Buat Account Baru !</h1>
    <form action="/welcome" method="post">
      @csrf
      <fieldset>
        <h4>Sign Up Form</h4>
        <form >

          <div class="row">
            <div class="col">
              <input type="text" name="depan"class="form-control" placeholder="Nama Depan">
            </div>
            <div class="col">
              <input type="text" name="belakang" class="form-control" placeholder="Nama Belakang">
            </div>
          </div>
        </form>
        <p>Gender :</p>
        <div class="form-check form-check-inline">
          <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="male">
          <label class="form-check-label" for="inlineRadio1">Male</label>
        </div>
        <div class="form-check form-check-inline">
          <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="female">
          <label class="form-check-label" for="inlineRadio1">Female</label>
        </div>
        <div class="form-check form-check-inline">
          <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="other">
          <label class="form-check-label" for="inlineRadio1">Other</label>
        </div>

        <p>Nationality :</p>
        <div class="form-check form-check-inline">
        <select class="form-control form-control-lg" name="negara">
          <option value="Jepang">Jepang</option>
          <option value="Indonesia">Indonesia</option>
          <option value="Jamaika">Jamaika</option>
        </select>
      </div>

        <p>Language Spoken :</p>
        <div class="form-check form-check-inline">
          <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="indonesia">
          <label class="form-check-label" for="inlineCheckbox1">Bahasa Indonesia</label>
        </div>
        <div class="form-check form-check-inline">
          <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="bali">
          <label class="form-check-label" for="inlineCheckbox1">Bahasa Bali</label>
        </div>
        <div class="form-check form-check-inline">
          <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="inggris">
          <label class="form-check-label" for="inlineCheckbox1">Bahasa Inggris</label>
        </div>

          <p>Bio :</p>
          <textarea required name="bio" rows="15" cols="100"></textarea><br>

          <div class="form-group row">
          <div class="col-sm-10">
            <button type="submit" class="btn btn-primary">Sign in</button>
          </div>
        </div>
      </fieldset>
      <p><em>Created by Nengah Sumerta</em> </p>
    </form>
  </div>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
  </body>
</html>
