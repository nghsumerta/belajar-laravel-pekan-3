<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
  public function daftar(){
    return view('register');
  }

  public function welcom(){
    return view ('welcome');
  }
  public function handleDaftar(Request $request){
    //dd($request)->all());
    $namadepan = $request["depan"];
    $namabelakang = $request["belakang"];
    return view ('welcome', compact('namadepan','namabelakang'));
  }
}
